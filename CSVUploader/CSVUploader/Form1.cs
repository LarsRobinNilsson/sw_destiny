﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CSVUploader
{
	public partial class Form1 : Form
	{
		public string Filename { get; set; }
		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				Filename = openFileDialog1.FileName;
				textBox1.AppendText(Filename);
			}
		}

		private void button1_Click_1(object sender, EventArgs e)
		{
			Program.ReadAndUpload(Filename);
			this.Close();
		}
	}
}
