﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace CLI__only_backend_.Classes
{
	/// <summary>
	/// Class for handeling of single cards.
	/// </summary>
	public class Card
	{
		#region Fields
		private Dice _dice;
		#endregion

		#region Constructor
		/// <summary>
		/// Finds card in the database and loads the data
		/// </summary>
		/// <param name="nr">Cardnumber</param>
		/// <param name="set">Cardset (Awakening/SoR)</param>
		public Card(int nr, string set)
		{
			using (MySqlConnection conn = DB.GetMySqlConnection())
			{
				using (MySqlCommand cmd = conn.CreateCommand())
				{
					//cmd.Connection = conn;
					string sql = @"	SELECT cs.set, c.id, c.nr, c.name, c.affiliation, c.color, c.cost, c.text, COUNT(*) AS rows
							FROM cards as c
							JOIN cardset as cs
							ON c.setid = cs.id
							WHERE cs.set LIKE @pSetId AND c.nr = @pNr";
					cmd.CommandText = sql;
					cmd.Parameters.AddWithValue("@pSetId", $"%{set}%");
					cmd.Parameters.AddWithValue("@pNr", nr);

					MySqlDataReader rdr = cmd.ExecuteReader();

					rdr.Read();
					int rows = Int32.Parse(rdr["rows"].ToString());

					if (rows > 1)
					{
						throw new Exception("More than one match in DB");
					}
					else if (rows == 0)
					{
						throw new Exception("No hits in database");
					}
					if (rows == 1)
					{
						Set = rdr["set"].ToString();
						Nr = Int32.Parse(rdr["nr"].ToString());
						Id = Int32.Parse(rdr["id"].ToString());
						Name = rdr["name"].ToString();
						Affi = rdr["affiliation"].ToString();
						Color = rdr["set"].ToString();
						Cost = Int32.Parse(rdr["cost"].ToString());
						CardText = rdr["text"].ToString();
					}

					rdr.Close();
				}
			}
		}

		/// <summary>
		/// Gets values from database-reader.
		/// </summary>
		/// <param name="rdr">Datareader from the card-table</param>
		public Card(MySqlDataReader rdr)
		{
			Id = Int32.Parse(rdr["nr"].ToString());
			Set = rdr["set"].ToString();
			Name = rdr["name"].ToString();
			Affi = rdr["affiliation"].ToString();
			Color = rdr["color"].ToString();
			Cost = Int32.Parse(rdr["cost"].ToString());
			CardText = rdr["text"].ToString();
		}
		#endregion

		#region Properties
		public string Name { get; set; }
		public int Cost { get; set; }
		/// <summary>
		/// Row from the database
		/// </summary>
		public int Id { get;}
		public int Nr { get;}
		public string Set { get;}
		public string CardText { get;}
		public Dice Dice { get => _dice; }
		public string Affi { get;}
		public string Color { get;}
		#endregion

		#region methods
		public override string ToString()
		{
			string outText = $"{Set} {Nr}\n{Name} ({Cost},{Color},{Affi})\n{CardText}\n";
			return outText;
		}
		#endregion
	}
}
