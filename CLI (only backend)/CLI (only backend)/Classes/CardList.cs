﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLI__only_backend_.Classes
{
	/// <summary>
	/// Class for cardlists
	/// </summary>
	public abstract class CardList
	{
		#region Fields
		private List<Card> _cards = new List<Card>();
		#endregion

		#region Propteries
		public List<Card> Cards
		{
			get => _cards; set => _cards = value;
		}
		public int? Id { get; set; }
		public bool CardsLoaded { get; set; }
		#endregion

		public CardList()
		{
			Cards = new List<Card>();
		}

		#region Methods
		/// <summary>
		/// Returns info about each card.
		/// </summary>
		/// <returns>Multiline string listing the name of each carc.</returns>
		public override string ToString()
		{
			string longLine = null;
			if (Cards.Count > 0)
			{
				longLine += "Cards:\n";
				foreach (var card in Cards)
				{
					longLine += card.ToString();
				}
			}
			return longLine;
		}
		public void FilterList()
		{
			throw new NotImplementedException();
		}
		public void SortList()
		{
			throw new NotImplementedException();
		}
		/// <summary>
		/// Adds a card to the list
		/// </summary>
		/// <param name="nr">Cardnumber</param>
		/// <param name="set">Cardset</param>
		public void AddCard(int nr, string set)
		{
			Cards.Add(new Card(nr, set));
		}
		/// <summary>
		/// Removes a card from the list
		/// </summary>
		/// <param name="id">Card id</param>
		public void RemoveCard(int id)
		{
			var card = Cards.FirstOrDefault(c => c.Id == id);
			Cards.Remove(card);
		}
		/// <summary>
		/// Printer-friendly card-list
		/// </summary>
		public void PrintList()
		{
			throw new NotImplementedException();
		}
	
		#endregion
	}
}
