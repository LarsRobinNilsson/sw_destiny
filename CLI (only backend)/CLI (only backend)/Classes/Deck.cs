﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CLI__only_backend_.Interfaces;
namespace CLI__only_backend_.Classes
{
	/// <summary>
	/// Class for decks, child of Cardlist
	/// </summary>
	class Deck : CardList, IListDB
	{
		#region fields
		string _name;
		#endregion

		#region constructor
		public Deck(string name, string creator = null, string desc = null)
		{
			Name = name;
			Creator = creator;
			Desc = desc;
			Cards = new List<Card>();

		}

		public Deck() { }
		#endregion

		#region properties
		public string Name
		{
			get => _name;
			set
			{
				if(value.Trim() == "" || value.Trim() == null)
				{
					throw new Exception("Name can't be null");
				}
				else
				{
					_name = value.Trim();
				}
			}
			
		}
		public string Creator { get; set; }
		public string Desc { get; set; }
		#endregion

		#region methods
		/// <summary>
		/// Creates a printer/screen-friendly string of the data
		/// </summary>
		/// <returns>Printerfriendly string</returns>
		public override string ToString()
		{
			string outString = $"Name: {Name}({Creator})\n{Desc}\n";			
			outString += base.ToString();
			return outString;
		}
		/// <summary>
		/// Adds a new deck to the database
		/// </summary>
		public void InsertCardList()
		{
			//Kanske borde retturnera true/false
			if (Name == null || Name == "")
			{
				throw new ArgumentException("The deck must have a name to be saved.");
			}

			//Skriv användarhantering.
			using (MySqlConnection conn = DB.GetMySqlConnection())
			{
				using (MySqlCommand cmd = conn.CreateCommand())
				{
					//cmd.Connection = conn;
					string sql = @"INSERT INTO decks (creator, name, description) VALUES (@creator, @name, @desc);SELECT SCOPE_IDENTITY()";
					cmd.CommandText = sql;
					cmd.Parameters.AddWithValue("@name", Name);
					cmd.Parameters.AddWithValue("@creator", Creator);
					cmd.Parameters.AddWithValue("@desc", Desc);

					try
					{
						Id = Int32.Parse(cmd.ExecuteScalar().ToString());
						Console.WriteLine($"Deck {Name} created.");
					}
					catch
					{
						throw new Exception($"A deck with the name '{Name}' already exists in the database");
					}
					
				}
			}
		}
		/// <summary>
		/// Updates and existing deck
		/// </summary>
		public void UpdateCardList()
		{
			//Kanske borde retturnera true/false
			if (Name == null || Name == "")
			{
				throw new ArgumentException("The deck must have a name to be saved.");
			}

			//Skriv användarhantering.
			using (MySqlConnection conn = DB.GetMySqlConnection())
			{
				using (MySqlCommand cmd = conn.CreateCommand())
				{
					//cmd.Connection = conn;
					string sql = @"	UPDATE decks SET 
									creator = @creator,
									name = @name,
									description = @desc
									WHERE id = @id";
					cmd.CommandText = sql;
					cmd.Parameters.AddWithValue("@name", Name);
					cmd.Parameters.AddWithValue("@creator", Creator);
					cmd.Parameters.AddWithValue("@desc", Desc);
					cmd.Parameters.AddWithValue("@id", Id);

					try
					{
						cmd.ExecuteNonQuery();
						Console.WriteLine($"Deck {Name} updated.");
					}
					catch
					{
						throw new Exception($"Update failed");
					}

				}
			}
		}		
		/// <summary>
		/// Searches the object for and card, true if not found.
		/// </summary>
		/// <param name="card">Card to search for</param>
		/// <returns>True if card does not exsit in the deck</returns>
		public bool IsCardNew(Card card)
		{
			using (MySqlConnection conn = DB.GetMySqlConnection())
			{
				using (MySqlCommand cmd = conn.CreateCommand())
				{
					//cmd.Connection = conn;
					string sql = @"	SELECT deckid, number
									FROM cardsindeck
									WHERE deckid = @deckid
									AND cardid = @cardid";
					cmd.CommandText = sql;
					cmd.Parameters.AddWithValue("@deckid", this.Id);
					cmd.Parameters.AddWithValue("@cardid", card.Id);

					try
					{
						MySqlDataReader rdr = cmd.ExecuteReader();
						rdr.Read();
						int dbNr = Int32.Parse(rdr["number"].ToString());
						rdr.Close();
						if (dbNr != 1) //Ska fixas med funktionalitet för att uppdatea nummer
						{
							return false;
						}
						//Oförändrat kort
						else
						{
							return false;
						}
					}
					catch
					{
						return true;
					}
				}
			}
		}
		/// <summary>
		/// Loads a deck from the database
		/// </summary>
		public void LoadCardList()
		{
			using (MySqlConnection conn = DB.GetMySqlConnection())
			{
				using (MySqlCommand cmd = conn.CreateCommand())
				{
					string sql = @"	SELECT id FROM decks
									WHERE name = @name";
					cmd.CommandText = sql;
					cmd.Parameters.AddWithValue("@name", Name);

					Id = Int32.Parse(cmd.ExecuteScalar().ToString());
				}
			}
		}
		/// <summary>
		/// Deletes the relation between the card and deck
		/// </summary>
		/// <param name="card">Card to remove</param>
		public void RemoveCardFromDB(Card card)
		{
			using (MySqlConnection conn = DB.GetMySqlConnection())
			{
				using (MySqlCommand cmd = conn.CreateCommand())
				{
					string sql = @"	DELETE FROM cardsindeck
									WHERE cardId = @cardId
									AND id = @id";
					cmd.CommandText = sql;
					cmd.Parameters.AddWithValue("@cardId", card.Id);
					cmd.Parameters.AddWithValue("@id", Id);

					cmd.ExecuteNonQuery();
				}
			}
		}
		/// <summary>
		/// Loads the cards from the dabatse and adds them to the deck as card-objects
		/// </summary>
		public void LoadCards()
		{
			if (!CardsLoaded)
			{
				//Kanske borde retturnera true/false
				using (MySqlConnection conn = DB.GetMySqlConnection())
				{
					using (MySqlCommand cmd = conn.CreateCommand())
					{
						string sql = @"	SELECT c.*, cid.id,cs.set
									FROM cardsindeck as cid	
									JOIN decks as d
									ON cid.deckid = d.id
									JOIN cards as c
									on cid.cardid = c.id
                                    JOIN cardset as cs
                                    ON cs.id = c.setid
									WHERE d.id = @id";
						cmd.CommandText = sql;
						cmd.Parameters.AddWithValue("@id", Id);

						MySqlDataReader rdr = cmd.ExecuteReader();

						if (!rdr.HasRows)
						{
							//InDB = true;
							throw new Exception("No cards saved for the deck");
						}

						while (rdr.Read())
						{
							Cards.Add(new Card(rdr));
						}
						CardsLoaded = true;
						rdr.Close();
					}
				}
			}
		}
		/// <summary>
		/// Uppdates an existing connection between a deck and card
		/// </summary>
		/// <param name="card"></param>
		public void UpdateCard(Card card)
		{
			if (Id == null)
			{
				throw new Exception("No Id set, load the deck from the database");
			}
			if (Cards.Count == 0)
			{
				throw new Exception("There are no cards to save");
			}

			using (MySqlConnection conn = DB.GetMySqlConnection())
			{
				using (MySqlCommand cmd = conn.CreateCommand())
				{
					string sql = @"UPDATE cardsindeck SET number=@numb WHERE deckid = @deckid AND cardid = @cardid";
					cmd.CommandText = sql;
					cmd.Parameters.AddWithValue("@numb", "1"); //Needs to be fixed
					cmd.Parameters.AddWithValue("@deckid", this.Id);
					cmd.Parameters.AddWithValue("@cardid", card.Id);
					cmd.ExecuteNonQuery();
					cmd.Parameters.Clear();
				}
			}
		}
		/// <summary>
		/// Creates a new reletion between deck and card
		/// </summary>
		/// <param name="card"></param>
		public void InsertCard(Card card)
		{
			if (Id == null)
			{
				throw new Exception("No Id set, load the deck from the database");
			}
			if (Cards.Count == 0)
			{
				throw new Exception("There are no cards to save");
			}

			using (MySqlConnection conn = DB.GetMySqlConnection())
			{
				using (MySqlCommand cmd = conn.CreateCommand())
				{
					string sql = "INSERT INTO cardsindeck(deckid, cardid, number) VALUES(@deckid,@cardid,@numb)";
					cmd.CommandText = sql;
					cmd.Parameters.AddWithValue("@numb", "1"); //Needs to be fixed
					cmd.Parameters.AddWithValue("@deckid", this.Id);
					cmd.Parameters.AddWithValue("@cardid", card.Id);
					cmd.ExecuteNonQuery();
					cmd.Parameters.Clear();
				}
			}
		}
		/// <summary>
		/// Deletes the deck and associated cardlist from the table and emtpies the cardlist in the object.
		/// </summary>
		public void RemoveCardListFromDB()
		{
			if(Id != null)
			{
				using (MySqlConnection conn = DB.GetMySqlConnection())
				{
					using (MySqlCommand cmd = conn.CreateCommand())
					{
						string sql;
						//Removes deck and all associated cards.
							sql = @"DELETE FROM cardsindeck	WHERE deckid	= @id
									;DELETE FROM decks		WHERE id		= @id";						
						cmd.CommandText = sql;
						cmd.Parameters.AddWithValue("@id", Id);
					
							cmd.ExecuteNonQuery();						
					}
					//Removes from the objekt
					Cards = null;
					CardsLoaded = false;
				}
			}
			else
			{
				throw new Exception("Nått");
			}
		}
		#endregion
	}
}
