﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CLI__only_backend_.Classes;

namespace CLI__only_backend_
{
	/// <summary>
	/// Standin for GUI, to test the back-end code.
	/// </summary>
	class Program
	{
		static void Main(string[] args)
		{
			ShowMainMenu();
		}

		static void ShowMainMenu()
		{
			Console.Clear();
			Console.WriteLine("Star Wars Destiny Menu:\n----------");
			Console.WriteLine(	"1) Show all cards\n" +
								"2) Manage my collection\n" +
								"3) Manage Decks\n" +
								"4) List all decks\n"+
								"11) Create card (test)\n"+
								"10) Exit");

			Int32.TryParse(Console.ReadLine(), out int val);
			try
			{
				Console.Clear(); 

				switch (val)
				{
					//Show all cards
					case 1:
						Console.WriteLine("Show all cards:\n----------");
						ShowCardList();
						Console.WriteLine("-End of list-");
						break;
					case 2:
						Console.WriteLine("Manage my collection:\n----------");
						throw new NotImplementedException("Not done yet");
						//Console.ReadKey();
						//break;
					//Create new deck
					case 3:
						Console.WriteLine("Menage decks:\n----------");
						DeckMenu();
						break;
					case 4:
						ListAllDecks();
						break;
					case 10:
						Console.WriteLine("Exiting program:\n----------");
						Console.WriteLine("Exiting program");
							break;
					case 11:
						Console.WriteLine("Create a card objekt (test):\n----------");
						Console.WriteLine("Set: (Awakening/Spirit of Rebellion)");
						string set = Console.ReadLine();
						Console.WriteLine("Cardnumber: ()");
						Int32.TryParse(Console.ReadLine(), out int id);
						Console.WriteLine(new Card(id, set).ToString());
						break;
					default:
						Console.WriteLine("Error, unvalid option");
						break;
				}
				Console.ReadKey();
			}
				catch (Exception e)
				{
					Console.WriteLine(e.Message);
					Console.ReadKey();
				}

			if (val != 10)
			{
				ShowMainMenu();
			}
		}

		private static void ListAllDecks()
		{
			Console.Clear();
			Console.WriteLine("List all decks:\n----------");
			var listDeck = Stuff.GetAllDecks();
			foreach (var d in listDeck)
			{
				try
				{
					d.LoadCards();
				}
				catch (Exception e)
				{
					Console.WriteLine(e.Message);
				}
				Console.WriteLine(d.ToString());
			}
			Console.WriteLine("-End of list-");
		}

		private static void DeckMenu(Deck deck = null)
		{
			while (deck == null)
			{
				Console.WriteLine("Deck:");
				string deckName = Console.ReadLine();
				try
				{
					deck = new Deck()
					{
						Name = deckName
					};
				}
				catch (Exception e)
				{
					Console.WriteLine(e.Message);
				}
				try
				{
					deck.LoadCardList();
					deck.LoadCards();
				}
				catch { }
			}
			Console.Clear();
			
			Console.WriteLine(deck.ToString());
			
			
			Console.WriteLine(	"1) Edit Deck\n" +
								"2) Add cards to deck\n" +
								"3) Remove cards from deck\n" +
								"7) Save deck\n" +
								"8) Delete deck\n" +
								"10) Done");

			Int32.TryParse(Console.ReadLine(), out int val);
			Console.Clear();
			switch (val)
			{
				case 1:					
					EditDeckMenu(deck);					
					break; 						
				case 2:
					Console.WriteLine("Add card to deck:\n----------");
					AddCardToDeckMenu(deck);
					break;
				case 3:
					Console.WriteLine("Remove card from deck:\n----------");
					RemoveCardFromDeckMenu(deck);
					break;
				case 7:
					Console.WriteLine("Save new deck:\n----------");
					CreateDeckMenu(deck);
					break;
				case 8:
					Console.WriteLine("Delete deck:\n----------");
					RemoveDeckMenu(deck);
					break;
				case 10:
					Console.WriteLine("Returning to main menu");
					break;
				default:
					Console.WriteLine("Error, unvalid option");
					break;
			}
			Console.ReadKey();
			if (val != 10)
			{
				DeckMenu(deck);
			}
		}

		private static void RemoveCardFromDeckMenu(Deck deck)
		{
			Console.WriteLine("Card set: (Awakening/Spirit of Rebellion)\nType 'done' if done.");
			string set = Console.ReadLine();
			if (set != "done")
			{
				Console.WriteLine("Card nr:");
				int nr = Int32.Parse(Console.ReadLine());
				var card = new Card(nr, set);

				deck.RemoveCard(card.Id);
				deck.RemoveCardFromDB(card);
				RemoveCardFromDeckMenu(deck);
			}					
		}

		private static void AddCardToDeckMenu(Deck deck)
		{
			Console.WriteLine("Card set: (Awakening/Spirit of Rebellion)\nType 'done' if done.");
			string set = Console.ReadLine();

			if (set != "done")
			{
				Console.WriteLine("Card nr:");
				Int32.TryParse(Console.ReadLine(), out int nr);
				try
				{
					deck.AddCard(nr, set);

					Console.WriteLine(deck.Cards.LastOrDefault());
				}
				catch (Exception e)
				{
					Console.WriteLine(e.Message);
				}

				AddCardToDeckMenu(deck);
			}			
		}

		private static void RemoveDeckMenu(Deck deck)
		{
			Console.Clear();
			Console.WriteLine("Delete deck:\n----------");
			deck.RemoveCardListFromDB();
			deck = null;
			Console.WriteLine("Done");
		}
		private static void EditDeckMenu(Deck deck)
		{
			Console.WriteLine("Sett deck-data:\n----------");
			Console.WriteLine($"Deckname: ({deck.Name})");
			string name = Console.ReadLine();
			Console.WriteLine($"Creator: ({deck.Creator})");
			string creator = Console.ReadLine();
			Console.WriteLine($"Description ({deck.Desc})");
			string desc = Console.ReadLine();

			if (!String.IsNullOrEmpty(name))
			{
				deck.Name = name;
			}
			if (!String.IsNullOrEmpty(creator))
			{
				deck.Creator = creator;
			}
			if (!String.IsNullOrEmpty(desc))
			{
				deck.Desc = desc;
			}
		}

		private static void CreateDeckMenu(Deck deck)
		{
			Console.Clear();
			Console.WriteLine("Saves deck:\n----------");
			if (deck.Id == null)
			{
				deck.InsertCardList();
			}
			else
			{
				deck.UpdateCardList();
			}
			foreach (var card in deck.Cards)
			{
				if(deck.IsCardNew(card))
				{
					deck.InsertCard(card);
				}
				else
				{
					deck.UpdateCard(card);
				}
			}
			Console.WriteLine("Done");
		}

		private static void ShowCardList()
		{
			var cl = new CardLibary();

			foreach (var card in cl.Cards)
			{
				Console.WriteLine(card.ToString());
			}
			
		}
	}
}
