﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIck_of_Destiny.Classer
{
	/// <summary>
	/// Testdata, vill be changed for database-connections as I get that to work.
	/// </summary>
	class AddTestData
	{
		#region methods
		public static string[] AddDiceSides()
		{
			var strings = new String[6] { "Ett","Två","Tre","Fyra","Fem","Sex"};			
			return strings;
		}
		public static string AddCardName(Random rand)
		{
			var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			var stringChars = new char[8];
			
			for (int i = 0; i < stringChars.Length; i++)
			{
				stringChars[i] = chars[rand.Next(chars.Length)];
			}

			return new String(stringChars);
		}
		public static int AddCardCost(Random rand)
		{
			return rand.Next(1, 20);
		}
		#endregion
	}
}
