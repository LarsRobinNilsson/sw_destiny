﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIck_of_Destiny.Classer
{
	/// <summary>
	/// Class for decks, child of Cardlist
	/// </summary>
	class Deck : CardList
	{
		public Deck()
		{
			CharacterPoints = 30;
			NrCards = 30;
			Battlefield = 1;
		}

		public int CharacterPoints { get; set; }
		public int NrCards { get; set; }
		public int Battlefield { get; set; }

		public string Owner { get; set; }

		public override string ToString()
		{
			string outString = $"Ägare: {Owner}\n";
			outString += base.ToString();
			return outString;	
		}

		/// <summary>
		/// Adds another card to the list
		/// </summary>
		/// <param name="id">Cardnumber</param>
		/// <param name="set">Set</param>
		/// <param name="rand">Random for testruns</param>
		public void AddCard(int id, string set, Random rand = null)
		{
			if (rand == null)
			{
				Card c = new Card(id, set);
				Cards.Add(c);
			}
			//För tester, ska bort sen.
			else
			{
				Card c = null;
				for (int i = 0; i < 30; i++)
				{
					c = new Card(1, "AoS", rand);
					Cards.Add(c);
				}
			}
		}
	}
}
