﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIck_of_Destiny.Classer
{
	/// <summary>
	/// Class for single dice
	/// </summary>
	public class Dice
	{
		#region Constructor
		public Dice()
		{
			Sides = AddTestData.AddDiceSides();
		}
		#endregion

		#region properties
		public string[] Sides { get; }
		public int Id { get; }
		public string Set { get;}
		#endregion

		#region methods
		/// <summary>
		/// Returns a random side of the dice
		/// </summary>
		/// <returns>Rolled side (string)</returns>
		public string Roll(Random Rand)
		{
			int r = Rand.Next(1, Sides.Length+1);
			return Sides[r];
		}

		/// <summary>
		/// Retruns a printerfriendlt summary of the dice.
		/// </summary>
		/// <returns>(string)</returns>
		public override string ToString()
		{
			string longLine = null;

			for (int i = 0; i < Sides.Length; i++)
			{
				longLine += $"{i}: {Sides[i]} \n";
			}
			
			return longLine;
		}
		#endregion
	}
}
