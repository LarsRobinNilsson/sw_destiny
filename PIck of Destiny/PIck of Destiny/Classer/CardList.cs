﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIck_of_Destiny.Classer
{
	/// <summary>
	/// Class for cardlists
	/// </summary>
	class CardList
	{
		#region Fields
		private List<Card> _cards = new List<Card>();
		#endregion

		#region Constructor
		public CardList()
		{
			
		}
		#endregion

		#region Propteries
		public List <Card> Cards
		{
			get => _cards; set => _cards = value;
		}
		#endregion

		#region Methods
		/// <summary>
		/// Returns info about each card.
		/// </summary>
		/// <returns>Multiline string listing the name of each carc.</returns>
		public override string ToString()
		{
			string longLine = null;

			for (int i = 0; i < Cards.Count; i++)
			{
				longLine += $"{i}: {Cards[i].ToString()} \n";
			}

			return longLine;
		}

		public void FilterList()
		{ }
		public void SortList()
		{ }
		public void ShowCards()
		{ }
		#endregion
	}
}
