﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIck_of_Destiny.Classer
{
	/// <summary>
	/// Class for handeling of single cards.
	/// </summary>
	public class Card
	{
		#region Fields
		private Dice _dice;
		#endregion

		#region Constructor
		public Card(int id, string set, Random rand = null)
		{
			this.Id = id;
			this.Set = set;

			//Ska skrivas om till databaskoppling
			if (rand != null)
			{
			
			var dice = new Dice();
			}
			
		}
		#endregion

		#region Properties
		public string Name { get; set; }
		public int Cost { get; set; }
		public int MyProperty { get; set; }
		public int Id { get; }
		public string Set { get;}
		public string CardText { get; set; }
		public Dice Dice { get => _dice; }
		#endregion

		#region methods
		public override string ToString()
		{
			string outText = $"Name:{Name}, Cost:{Cost}\n {Id} - {Set}";

			//Tillfällig
			outText += Dice.ToString();
			return outText;
		}
		#endregion
	}
}
