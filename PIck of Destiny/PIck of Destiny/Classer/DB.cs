﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIck_of_Destiny.Classer
{
	public class DB
	{
		#region Fields
		private MySqlConnection _conn = null;
		#endregion

		#region Constructor
		public DB()
		{
			try
			{
				//Värdelös lösning
				string cs = @"server=mysql579.loopia.se;user id=reax@h176174;password=Bm3SQmUX;database=hourglass_nu_db_1";
				//Får inte det nedre att fungera.
				//string cs = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;

				_conn = new MySqlConnection(cs);
				_conn.Open();
			}
			catch (MySqlException ex)
			{
				Console.WriteLine("Error: {0}", ex.ToString());
			}
		}
		#endregion

		#region Destructor
		~DB()
		{
			_conn.Close();
		}
		#endregion

		#region Methods
		public void ReadCardFromDB(Card card)
		{
			MySqlDataReader rdr = null;
			var cmd = new MySqlCommand();
			cmd.Connection = _conn;

			string sql = "SELECT * FROM cards WHERE setid = @pSetId AND nr = @pNr";
			cmd.CommandText = sql;
			cmd.Parameters.AddWithValue("@pSetId", card.Set);
			cmd.Parameters.AddWithValue("@pNr", card.Id);

			rdr = cmd.ExecuteReader();
			int nRows = rdr.RecordsAffected;

			var cards = new Card[nRows];
			int n = 0;
			while(rdr.Read())
			{
				
				n++; 
			}
		}
		public void WriteToDB(MySqlCommand cmd)
		{
			cmd.Connection = _conn;
			cmd.ExecuteNonQuery();
		}
		#endregion
	}
}
